from flask import Flask
from flask import render_template
from ModKullanici.yönetici import mod_Kullanici

app = Flask(__name__)
app.register_blueprint(mod_Kullanici, url_prefix='/Kullanici')


@app.route('/')
def hello_world():
    return render_template('sablon.html')


if __name__ == '__main__':
    app.run()
