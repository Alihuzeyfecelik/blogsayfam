from app import app
from flask import render_template, Blueprint


mod_Kullanici = Blueprint('Kullanici', __name__)

@mod_Kullanici.route('/login')
def KullaniciLogin():
    return render_template('/KullaniciLogin')

@mod_Kullanici.route('/Liste')
def KullaniciListe():
    return render_template('sablon.html')

@mod_Kullanici.route('/kayıtol')
def kayıtol():
    return render_template('kayitol.html')

