from flask import Blueprint, render_template

ModYazıcı = Blueprint('ders', __name__)

@ModYazıcı.route('/liste')
def yazıcıListe():
    return render_template('sablon.html')